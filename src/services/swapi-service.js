export default class SwapiService {

  _apiBase = 'https://swapi.dev/api';
  _imageBase = 'https://starwars-visualguide.com/assets/img';




  getResource = async (url) => {

    return fetch(`${this._apiBase}${url}`)
    .then((res)=>{
        return res.json();
    })
    .catch((er)=>{
      console.log((`Could not fetch ${url}` +
      `, received ${er}`));
    });


  }



  getAllPeople = async () => {
    return this.getResource(`/people/`)
    .then((res)=>{
      return res.results.map(this._transformPerson);
    });

  }

  getAllPeople = async () => {
    return this.getResource(`/people/`)
    .then((res) => {
      return res.results
      .map(this._transformPerson)
      .slice(0, 5);
    });
    
  };

  getPerson = async (id) => {
    return this.getResource(`/people/${id}/`)
    .then((person) => this._transformPerson(person));
    
  };

  getAllPlanets = async () => {
    return this.getResource(`/planets/`)
    .then((res)=>{
      return res.results
      .map(this._transformPlanet)
      .slice(0, 5);
    });
    
  };

  getPlanet = async (id) => {
    return this.getResource(`/planets/${id}/`)
    .then((planet) => this._transformPlanet(planet));
    
  };

  getAllStarships = async () => {
   return this.getResource(`/starships/`)
   .then((res)=>{
    return res.results
    .map(this._transformStarship)
    .slice(0, 5);
   })
    
  };

  getStarship = async (id) => {
    
    return this.getResource(`/starships/${id}/`)
    .then((starship) =>  this._transformStarship(starship))
    
  };

  getPersonImage = ({id}) => {
    return `${this._imageBase}/characters/${id}.jpg`
  };

  getStarshipImage = ({id}) => {
    return `${this._imageBase}/starships/${id}.jpg`
  };

  getPlanetImage = ({id}) => {
    return `${this._imageBase}/planets/${id}.jpg`
  };

  _extractId = (item) => {

    
    const idRegExp = /\/([0-9]*)\/$/;
    return item.url.match(idRegExp)[1];
  };

  _transformPlanet = (planet) => {
    return {
      id: this._extractId(planet),
      name: planet.name,
      population: planet.population,
      rotationPeriod: planet.rotation_period,
      diameter: planet.diameter
    };
  };

  _transformStarship = (starship) => {

    
    return {
      id: this._extractId(starship),
      name: starship.name,
      model: starship.model,
      manufacturer: starship.manufacturer,
      costInCredits: starship.cost_in_credits,
      length: starship.length,
      crew: starship.crew,
      passengers: starship.passengers,
      cargoCapacity: starship.cargo_capacity
    }
  };

  _transformPerson = (person) => {
    return {
      id: this._extractId(person),
      name: person.name,
      gender: person.gender,
      birthYear: person.birth_year,
      eyeColor: person.eye_color
    }
  }
}
