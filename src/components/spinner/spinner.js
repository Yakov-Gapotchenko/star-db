import React from 'react';
import './spinner.css';

export default class Spinner extends React.Component{
    render(){
        return (
            <div className="loadingio-spinner-disk-7o48i12hbo8">
                <div className="ldio-qqp9ayc5w5i">
                    <div>
                        <div></div>
                        <div></div>
                    </div>
                </div>
            </div>

        );
    }



}