import React, { Component } from 'react';
import { PersonDetails, PersonList } from '../sw-components';
import Row from '../row';
import {withRouter} from 'react-router-dom';


 const PeoplePage = ({match, history}) => {

  
    return (
      <Row
        left={<PersonList onItemSelected={(itemId) => history.push(itemId)}/>}
        right={<PersonDetails itemId={Number(match.params.id)} />}
        />
    );
  };

  export default withRouter(PeoplePage);



