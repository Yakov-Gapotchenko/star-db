import React, { Component } from 'react';

import Header from '../header';
import RandomPlanet from '../random-planet';
import ErrorBoundry from '../error-boundry';

import { PeoplePage, PlanetsPage, StarshipsPage,
LoginPage, SecretPage} from "../pages";

import SwapiService from '../../services/swapi-service';
import DummySwapiService from '../../services/dummy-swapi-service';

import { SwapiServiceProvider } from '../swapi-service-context';



import './app.css';

import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import { StarshipDetails } from '../sw-components';
import peoplePage from '../pages/people-page';


export default class App extends Component {

  state = {
    swapiService: new SwapiService(),
    isLoggedIn: false
  };

  onLogin = () => {
    this.setState({
      //isLoggedIn: ! state.isLoggedIn
      isLoggedIn: true
    });


  }

  onServiceChange = () => {
    this.setState(({ swapiService }) => {

      const Service = swapiService instanceof SwapiService ?
                        DummySwapiService : SwapiService;

      return {
        //swapiService: new SwapiService()
        swapiService: new Service()
      };
    });
  };



  render() {


    const {isLoggedIn} = this.state;


    return (
      <ErrorBoundry>
        <SwapiServiceProvider value={this.state.swapiService} >
          <Router>

          <div className="stardb-app">
            <Header onServiceChange={this.onServiceChange} />
            <RandomPlanet updateInterval={10000} />



            <Switch>
            <Route
             path="/" 
             render={()=> <h2> Welcome to StarDb! </h2>} 
             exact
             />
            <Route path="/people/:id?" exact component={PeoplePage}/>;
            <Route path="/planets" exact component={PlanetsPage} />
            <Route path="/starships" exact component={StarshipsPage} />
            
            <Route path="/starships/:id" 
              render={({match}) => {
                const id = Number(match.params.id) ;
                
                return <StarshipDetails itemId={id} />;
              }}
              
             />

            
            <Route 
            path="/login" 
            exact 
            render={() => (
              <LoginPage 
              onLogin={this.onLogin}
              isLoggedIn={isLoggedIn}
              />)
            } />
            <Route 
            path="/secret"
             exact 
             render={() => (
              <SecretPage isLoggedIn={isLoggedIn}/>
             )} />


              <Route render={() => <h2> Page not found </h2>} />
            </Switch>

          </div>
          </Router>
        </SwapiServiceProvider>
      </ErrorBoundry>
    );
  }
}
